package BibliotecaApp.util;

import BibliotecaApp.model.Carte;

import static org.apache.commons.lang3.StringUtils.length;

public class Validator {

    // functie booleana care verifica daca stringul trimis ca parametru contine numai litere din alfabetul englez
    public static boolean isStringOK(String s) throws Exception {
        boolean flag = s.matches("[a-zA-Z ]+");
        if (flag == false)
            throw new Exception("String invalid");
        return flag;
    }

    // valideaza structura interna a cartii c trimisa ca parametru
    public static void validateCarte(Carte c) throws Exception {
        if (!isStringOK(c.getTitlu()))
            throw new Exception("Titlu invalid!");
        if (length(c.getTitlu()) < 2 || length(c.getTitlu()) > 100) {
            throw new Exception("Titlu invalid! Lungimea trebuie sa fie cuprinsa intre 2 si 100.");
        }

        if (c.getAutori() == null) {
            throw new Exception("Lista autori vida!");
        }
        for (String s : c.getAutori()) {
            if (!isStringOK(s))
                throw new Exception("Autor invalid!");
        }

        if (length(c.getEditura()) < 5 || length(c.getEditura()) > 50) {
            throw new Exception("Editura incorecta! Lungimea trebuie sa fie cuprinsa intre 5 si 50.");
        }

        if (!isNumber(c.getAnAparitie()))
            throw new Exception("An invalid (ne-numeric)!"); // Exception("Editura invalid!");
        if (Integer.parseInt(c.getAnAparitie()) < 1800 || Integer.parseInt(c.getAnAparitie()) > 2018) {
            throw new Exception("An aparitie invalid! Valoarea trebuie sa fie cuprinsa intre 1800 si 2018.");
        }

        //if(c.getCuvinteCheie()!=null){ //if(c.getCuvinteCheie()==null){
        if (c.getCuvinteCheie() == null) {
            throw new Exception("Lista cuvinte cheie vida!");
        }
        for (String s : c.getCuvinteCheie()) {
            if (!isStringOK(s))
                throw new Exception("Cuvant cheie invalid!");
        }

    }

    // functie booleana care verifica daca stringul trimis ca parametru contine numai cifre
    public static boolean isNumber(String s) {
        return s.matches("[0-9]+");
    }
//
//	public static boolean isOKString(String s){
//		String []t = s.split(" ");
//		if(t.length==2){
//			boolean ok1 = t[0].matches("[a-zA-Z]+");
//			boolean ok2 = t[1].matches("[a-zA-Z]+");
//			if(ok1==ok2 && ok1==true){
//				return true;
//			}
//			return false;
//		}
//		return s.matches("[a-zA-Z]+");
//	}


}
