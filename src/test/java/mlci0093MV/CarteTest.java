package mlci0093MV;

import BibliotecaApp.model.Carte;
import org.junit.*;

import java.util.ArrayList;

import static org.junit.Assert.*;

public class CarteTest {
    Carte c;

    @BeforeClass  //config facute aici vor fi valabile pt toate testele; se apeleaza o singura data
    public static void setup() {
        System.out.println("before any test");
    }

    @AfterClass
    public static void teardown() {
        System.out.println("after all tests");
    }


    @Before //se apeleaza inaintea fiecarui test
    public void setUp() throws Exception {
        c = new Carte();
        c.setTitlu("Ion");

        ArrayList<String> Autori = new ArrayList<String>();
        Autori.add("Eliade");
        c.setAutori(Autori); //???

        ArrayList<String> CuvinteCheie = new ArrayList<String>();
        CuvinteCheie.add("Ala");
        CuvinteCheie.add("Bala");
        CuvinteCheie.add("Portocala");
        c.setCuvinteCheie(CuvinteCheie); //???


        System.out.println("Before test");
    }

    @After
    public void tearDown() throws Exception {
        c = null;

        System.out.println("After test");
    }


    @Test
    public void getTitlu() throws Exception {
        assertEquals( "Ion", c.getTitlu());
    }

    @Test
    public void setTitlu() throws Exception {
        c.setTitlu("Gheorghe");
        assertEquals("Gheorghe", c.getTitlu());
    }

    @Test // ???
    public void setAutori() throws Exception {
        ArrayList<String> Autori = new ArrayList<String>();
        Autori.add("Eliade");
        Autori.add("Creanga");

        c.setAutori(Autori); // aici se suprascrie vectorul de autori din @Before

        assertEquals(Autori, c.getAutori());
    }

    @Test(timeout = 1000)
    public void getAutori()  {
        // c.setAutori("Eliade");
        try {
            Thread.sleep(100);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        ArrayList<String> Autori = new ArrayList<String>();
        Autori.add("Eliade");

        assertEquals(Autori, c.getAutori());
    }

    @Test(expected = Exception.class)  //testul trece daca APARE exceptia
    public void setAnAparitie() throws Exception {
        c.setAnAparitie("1799");
    }

    // TEMA

    @Test
    public void deleteCuvinteCheie()  {


        c.deleteCuvantCheie("Portocala");


        ArrayList<String> CuvinteCheie = new ArrayList<String>();
        CuvinteCheie.add("Ala");
        CuvinteCheie.add("Bala");


        assertEquals(CuvinteCheie, c.getCuvinteCheie());
    }

    @Test
    public void cautaDupaAutor()  {

        assertEquals(true, c.cautaDupaAutor("Eliade"));
        assertEquals(false, c.cautaDupaAutor("Creanga"));

    }

    @Test
    public void deleteTotiAutorii()  {
        c.deleteTotiAutorii();

        assertEquals(false, c.cautaDupaAutor("Eliade"));
        assertEquals(false, c.cautaDupaAutor("Creanga"));

    }

    @Test(timeout = 2000)
    public void getEditura()  {

        try {
            Thread.sleep(200);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        c.setEditura("Humanitas");
        assertEquals("Humanitas", c.getEditura());
    }



}