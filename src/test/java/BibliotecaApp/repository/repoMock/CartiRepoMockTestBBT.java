package BibliotecaApp.repository.repoMock;

import BibliotecaApp.model.Carte;
//import BibliotecaApp.repository.repoMock.CartiRepoMock;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.*;

public class CartiRepoMockTestBBT {

    private CartiRepoMock crm;

    @Before
    public void setUp() throws Exception {
        crm = new CartiRepoMock();
    }

    @After
    public void tearDown() throws Exception {
    }

    @Test  (expected = Exception.class)  //testul trece daca APARE exceptia
    public void adaugaCarte0() throws Exception {
        crm.adaugaCarte(Carte.getCarteFromString("A;Popescu;2013;Dacia;bla, blabla, blablabla"));
    }

   // AB	Popescu	2012	Dacia	x, y,z	book added

    @Test
    public void adaugaCarte1() throws Exception {
        crm.adaugaCarte(Carte.getCarteFromString("AB;Popescu;2012;Dacia;x, y, z"));
    }

//   Titanic	Vasilescu	1980	Abcd	vas, vapor, ocean	eroare

    @Test (expected = Exception.class)  //testul trece daca APARE exceptia
    public void adaugaCarte2() throws Exception {
        crm.adaugaCarte(Carte.getCarteFromString("Titanic;Vasilescu;1980;Abcd;vas, vapor, ocean"));
    }

//  Titanic	Popescu	2000	Dacia	a, b, c	book added

    @Test
    public void adaugaCarte3() throws Exception {
        crm.adaugaCarte(Carte.getCarteFromString("Titanic;Popescu;2000;Dacia;a, b, c"));
    }

    // Titanic	Popescu	2000	Elevul	a,b,c	book added

    @Test
    public void adaugaCarte4() throws Exception {
        crm.adaugaCarte(Carte.getCarteFromString("Titanic;Popescu;2000;Elevul;a,b,c"));
    }

    // Primavara	Pop	1799	Dacia	boboc, soare	eroare

    @Test (expected = Exception.class)  //testul trece daca APARE exceptia
    public void adaugaCarte5() throws Exception {
        crm.adaugaCarte(Carte.getCarteFromString("Primavara;Pop;1799;Dacia;boboc, soare"));
    }

    // Primavara	Pop	1800	Dacia	boboc, soare	book added

    @Test
    public void adaugaCarte6() throws Exception {
        crm.adaugaCarte(Carte.getCarteFromString("Primavara;Pop;1800;Dacia;boboc,soare"));
    }

    // Scrisoarea I	Eminescu	2019	Humanitas	tara, 	eroare

    @Test (expected = Exception.class)  //testul trece daca APARE exceptia
    public void adaugaCarte7() throws Exception {
        crm.adaugaCarte(Carte.getCarteFromString("Scrisoarea I;Eminescu;2019;Humanitas;tara"));
    }

//    @Test
//    public void cautaCarte0() {
//        //List<Carte> carti = new ArrayList<Carte>();
//        assertEquals(0, crm.cautaCarte("Ionescu"));
//
//    }
//
//    @Test
//    public void cautaCarte1() {
//        List<Carte> cartigasite = new ArrayList<Carte>();
////        assertNotEquals(carti, cr.cautaCarte(null));
//
//       // crm.adaugaCarte(Carte.getCarteFromString("Poezii;Sadoveanu;1973;Corint;poezii"));
//
//        Carte c = new Carte();
//        c.setTitlu("Poezii");
//
//        ArrayList<String> Autori = new ArrayList<String>();
//        Autori.add("Sadoveanu");
//        c.setAutori(Autori); //???
//
//        try {
//            c.setAnAparitie("1973");
//        } catch (Exception e) {
//            e.printStackTrace();
//        }
//
//        c.setEditura("Corint");
//
//        ArrayList<String> CuvinteCheie = new ArrayList<String>();
//        CuvinteCheie.add("poezii");
//
//        try {
//            crm.adaugaCarte(c);
//        } catch (Exception e) {
//            e.printStackTrace();
//        }
//
//        //  Carte carte = new Carte("Poezii;Sadoveanu;1973;Corint;poezii");
//
//        cartigasite= crm.cautaCarte("Confucius");
//        assertEquals(0, cartigasite.size());
////        assertEquals(carti, null);
////        assertEquals(carti, (List<Carte>) null);
//    }
//
//    @Test
//    public void cautaCarte2() {
//        List<Carte> cartigasite = new ArrayList<Carte>();
////        assertNotEquals(carti, cr.cautaCarte(null));
//
//        // crm.adaugaCarte(Carte.getCarteFromString("Poezii;Sadoveanu;1973;Corint;poezii"));
//
//        // Carte 1
//        Carte c = new Carte();
//        c.setTitlu("Poezii");
//
//        ArrayList<String> Autori = new ArrayList<String>();
//        Autori.add("Sadoveanu");
//        c.setAutori(Autori); //???
//
//        try {
//            c.setAnAparitie("1973");
//        } catch (Exception e) {
//            e.printStackTrace();
//        }
//
//        c.setEditura("Corint");
//
//        ArrayList<String> CuvinteCheie = new ArrayList<String>();
//        CuvinteCheie.add("poezii");
//
//        try {
//            crm.adaugaCarte(c);
//        } catch (Exception e) {
//            e.printStackTrace();
//        }
//
//
//        // Carte 2
//        Carte cn = new Carte();
//        cn.setTitlu("Poezii");
//
//        ArrayList<String> Autori2 = new ArrayList<String>();
//        Autori2.add("Alecsandri");
//        cn.setAutori(Autori); //???
//
//        try {
//            cn.setAnAparitie("1900");
//        } catch (Exception e) {
//            e.printStackTrace();
//        }
//
//        cn.setEditura("Corint");
//
//        ArrayList<String> CuvinteCheie2 = new ArrayList<String>();
//        CuvinteCheie2.add("soare");
//
//        try {
//            crm.adaugaCarte(cn);
//        } catch (Exception e) {
//            e.printStackTrace();
//        }
//
//        //  Carte carte = new Carte("Poezii;Sadoveanu;1973;Corint;poezii");
//
//        cartigasite= crm.cautaCarte("Sadoveanu");
//        assertEquals(1, cartigasite.size());
////        assertEquals(carti, null);
////        assertEquals(carti, (List<Carte>) null);
//    }


//    @Test
//    public void cautaCarte3() {
//        List<Carte> carti = new ArrayList<Carte>();
////        carti = cr.getCarti();
//        carti.add(Carte.getCarteFromString("Poezii;Sadoveanu;1973;Corint;poezii"));
////        assertNotEquals(carti, cr.cautaCarte(null));
//        assertEquals(carti, crm.cautaCarte("Sadoveanu"));
////        assertEquals(carti, null);
////        assertEquals(carti, (List<Carte>) null);
//    }



}