package BibliotecaApp.repository.repoMock;

import BibliotecaApp.model.Carte;
//import BibliotecaApp.repository.repoMock.CartiRepoMock;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.assertEquals;

public class CartiRepoMockTestWBT {

    private CartiRepoMock crm;

    @Before
    public void setUp() throws Exception {
        crm = new CartiRepoMock();
    }

    @After
    public void tearDown() throws Exception {
    }

    @Test
    public void cautaCarte0() {
        List<Carte> carti = new ArrayList<Carte>();
        assertEquals(carti, crm.cautaCarte("Ionescu")); // carti = lista de carti goala; cu 0 sau null nu merge...

    }

    @Test
    public void cautaCarte1() {
        List<Carte> cartigasite = new ArrayList<Carte>();
//        assertNotEquals(carti, cr.cautaCarte(null));

        // crm.adaugaCarte(Carte.getCarteFromString("Poezii;Sadoveanu;1973;Corint;poezii"));

        Carte c = new Carte();
        c.setTitlu("Poezii");

        ArrayList<String> Autori = new ArrayList<String>();
        Autori.add("Sadoveanu");
        c.setAutori(Autori); //???

        try {
            c.setAnAparitie("1973");
        } catch (Exception e) {
            e.printStackTrace();
        }

        c.setEditura("Corint");

        ArrayList<String> CuvinteCheie = new ArrayList<String>();
        CuvinteCheie.add("poezii");

        try {
            crm.adaugaCarte(c);
        } catch (Exception e) {
            e.printStackTrace();
        }

        //  Carte carte = new Carte("Poezii;Sadoveanu;1973;Corint;poezii");

        cartigasite= crm.cautaCarte("Confucius");
        assertEquals(0, cartigasite.size());
//        assertEquals(carti, null);
//        assertEquals(carti, (List<Carte>) null);
    }

    @Test
    public void cautaCarte2() {
        List<Carte> cartigasite = new ArrayList<Carte>();
//        assertNotEquals(carti, cr.cautaCarte(null));

        // crm.adaugaCarte(Carte.getCarteFromString("Poezii;Sadoveanu;1973;Corint;poezii"));

        // Carte 1
        Carte c = new Carte();
        c.setTitlu("Poezii");

        ArrayList<String> Autori = new ArrayList<String>();
        Autori.add("Sadoveanu");
        c.setAutori(Autori); //???

        try {
            c.setAnAparitie("1973");
        } catch (Exception e) {
            e.printStackTrace();
        }

        c.setEditura("Corint");

        ArrayList<String> CuvinteCheie = new ArrayList<String>();
        CuvinteCheie.add("poezii");
        c.setCuvinteCheie(CuvinteCheie);

        try {
            crm.adaugaCarte(c);
        } catch (Exception e) {
            e.printStackTrace();
        }


        // Carte 2
        Carte cn = new Carte();
        cn.setTitlu("Poezii");

        ArrayList<String> Autori2 = new ArrayList<String>();
        Autori2.add("Alecsandri");
        cn.setAutori(Autori2); //???

        try {
            cn.setAnAparitie("1900");
        } catch (Exception e) {
            e.printStackTrace();
        }

        cn.setEditura("Corint");

        ArrayList<String> CuvinteCheie2 = new ArrayList<String>();
        CuvinteCheie2.add("soare");
        cn.setCuvinteCheie(CuvinteCheie2);

        try {
            crm.adaugaCarte(cn);
        } catch (Exception e) {
            e.printStackTrace();
        }

        //  Carte carte = new Carte("Poezii;Sadoveanu;1973;Corint;poezii");

//        List<Carte> cartinegasite = new ArrayList<Carte>();

        cartigasite = crm.cautaCarte("Sadoveanu");
        assertEquals(1, cartigasite.size());
//        assertEquals(carti, null);
//        assertEquals(carti, (List<Carte>) null);
//        assertEquals(cartinegasite, cartigasite);
    }

}